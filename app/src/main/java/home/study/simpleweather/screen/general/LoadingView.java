package home.study.simpleweather.screen.general;

public interface LoadingView {

    void showLoadingIndicator();

    void hideLoadingIndicator();

}
